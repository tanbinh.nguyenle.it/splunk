﻿using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.AspNetCore.Mvc;

namespace splunkDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SNSController : ControllerBase
    {
        private readonly ILogger<SNSController> _logger;
        private readonly IAmazonSimpleNotificationService _snsClient;
        private readonly IAmazonSQS _sqsClient;


        public SNSController(ILogger<SNSController> logger, IAmazonSimpleNotificationService snsClient, IAmazonSQS sqsClient)
        {
            _logger = logger;
            _snsClient = snsClient;
            _sqsClient = sqsClient;
        }

        [HttpPost("sendmessage")]
        public async Task<IActionResult> SendMessageToSQS(string message)
        {
            try
            {
                var publishRequest = new PublishRequest
                {
                    TopicArn = "arn:aws:sns:us-east-1:000000000000:AppEventTopic",
                    Message = message
                };

                var response = await _snsClient.PublishAsync(publishRequest);

                return Ok(response.MessageId);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("receivemessage")]
        public async Task<IActionResult> ReceiveMessageFromSQS()
        {
            try
            {
                // Nhận tin nhắn từ SQS
                var receiveMessageRequest = new ReceiveMessageRequest
                {
                    QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                    MaxNumberOfMessages = 1,
                    WaitTimeSeconds = 20
                };

                var response = await _sqsClient.ReceiveMessageAsync(receiveMessageRequest);
                
                if (response.Messages.Count > 0)
                {
                    var message = response.Messages[0].Body;
                    var receiptHandle = response.Messages[0].ReceiptHandle;

                    // Xử lý tin nhắn ở đây, ví dụ: trả về message và xoá tin nhắn khỏi hàng đợi
                    // ...

                    // Xoá tin nhắn sau khi đã xử lý
                    var deleteMessageRequest = new DeleteMessageRequest
                    {
                        QueueUrl = "http://localhost:4576/000000000000/AppEventQueue",
                        ReceiptHandle = receiptHandle
                    };

                    await _sqsClient.DeleteMessageAsync(deleteMessageRequest);

                    return Ok(message);
                }
                else
                {
                    return NotFound("No messages found in the queue.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}

